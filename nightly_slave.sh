#!/bin/bash

#Bash script to update, build and test opticks
# Author : Dr Keith Evans
# Date : 23/05/2022

export WORK=$(echo "/"$(hostname | head -c 7)"-data1/kevans")
export lhcb=$(echo "/"$(hostname | head -c 7)"-data1/kevans/lhcb-opticks/")
export yun=$(echo "/"$(hostname | head -c 7)"-data1/kevans/yunlongexample")
export OPTICKS_PYTHON=python3
export OPTICKS_GEOCACHE_PREFIX=$WORK

if [ $? != 0 ]; then
    echo "fail"
    exit 1
fi

# Source various environment configs

# source $WORK/lhcb-opticks/.opticks_config
source $WORK/.opticks_config
source /cvmfs/lhcb.cern.ch/lib/LbEnv
source $OPTICKS_PREFIX/bin/opticks-setup.sh 1> /dev/null
source $WORK/.geant4_select
source /cvmfs/lhcb.cern.ch/lib/lcg/releases/gcc/11.1.0/x86_64-centos7/setup.sh
export LD_LIBRARY_PATH=$WORK/usr/lib:$WORK/usr/lib64:$LD_LIBRARY_PATH
export INCLUDE=$WORK/usr/include:$INCLUDE
export PATH=$WORK/bin:$PATH

if [ $? != 0 ]; then
    echo "fail"
    exit 1
fi

#cd to opticks directory

opticks-scd
if [ $? != 0 ]; then
    echo "fail CD"
    exit 1
fi

# Check on master branch

# git checkout master
if [ $? != 0 ]; then
    echo "fail checkout"
    exit 1
fi

# update source to HEAD

git pull --verbose
if [ $? != 0 ]; then
    echo "fail pull"
    exit 1
fi

# begin build process

echo ""
echo $PATH
echo ""
echo $LD_LIBRARY_PATH
echo ""
echo $INCLUDE
echo ""

# oo
opticks-full
if [ $? != 0 ]; then
    echo "fail opticks-full"
    exit 1
fi

# # CD to example source directory

# cd $yun/opticks/Rich_Simplified
# if [ $? != 0 ]; then
#     echo "fail"
#     exit 1
# fi

# # Build Rich example

# ./build.sh clean
# if [ $? != 0 ]; then
#     echo "fail"
#     exit 1
# fi

# # CD to build directory to run test

# cd build
# if [ $? != 0 ]; then
#     echo "fail"
#     exit 1
# fi

# # run test

# ./Rich_Simplified
# if [ $? != 0 ]; then
#     echo "fail"
#     exit 1
# fi
