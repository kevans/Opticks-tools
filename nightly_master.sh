#!/bin/bash

# set date

DATE=$(date +'%d%m%y')

# Run build test saving output in the form a plain text file

/hepgpu9-data1/kevans/Opticks-tools/nightly_slave.sh > /hepgpu9-data1/kevans'/.nightly_logs/nightly_'$DATE'.txt'

# Determine if test was successful and prepare exit statuses 

EXITCODE=$?
echo "" >> /hepgpu9-data1/kevans'/.nightly_logs/nightly_'$DATE'.txt'
if [ $EXITCODE != 0 ]; then
    echo "****************************NIGHTLY BUILD FAILURE****************************" >> /hepgpu9-data1/kevans'/.nightly_logs/nightly_'$DATE'.txt'
    STATUS="Failure"
else
    echo "****************************NIGHTLY BUILD SUCCESS****************************" >> /hepgpu9-data1/kevans'/.nightly_logs/nightly_'$DATE'.txt'
    STATUS="Success"
fi  
echo "" >> /hepgpu9-data1/kevans'/.nightly_logs/nightly_'$DATE'.txt'

# Send plain text file to Keith

echo "Find attached the nightly build log of Opticks as of"$DATE | /bin/mailx -s "Nightly Test - "$STATUS -a /hepgpu9-data1/kevans'/.nightly_logs/nightly_'$DATE'.txt' keith.evans@manchester.ac.uk
